#!/usr/bin/env python3

n = input('Donnez-moi un nombre entier : ')
b = input('Quelle base vous intéresse (2-16) [2] : ')
b.strip()

if b == '':
    b = 2
else:
    b = int(b)

n = int(n)

digits = []
while n > 0:
    digits.insert(0, n % b)
    n = n // b

alldigits = '0123456789ABCDEF'

# C'est une liste en compréhension
# cf. le pdf du cours ou le manuel Python
# Voir aussi méthode "join" sur les chaîne (str)

print( ''.join([ alldigits[d] for d in digits ])  )
