#!/usr/bin/env python3

alldigits = '0123456789ABCDEF'

b = input('Entrez la base [2] : ').strip()
if b == '':
    b = 2
else:
    b = int(b)

n = input(f'Entrez un nombre en base {b} : ')

result = 0
# enumerate (voir manuel)
# on parcourt la chaîne à l'envers
for i, c in enumerate(n[::-1]):
    # find voir le manuel sur les chaîne
    val = alldigits.find(c)
    if val < b:
        result += val * b**i
    else:
        print(f'Erreur : {c} plus grand que {b-1} !')
        exit(42)
    
print('En décimal :', result)
