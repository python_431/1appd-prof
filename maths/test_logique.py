#!/usr/bin/env python3

# testez ce programme avec p or q 
# p and q
# p and (not q)

# demandez en plus de l'expression p1 une autre
# autre expression logique stockée dans la variable
# p2 et affichez une colonne en plus pour p2

# Dites à la fin si p1 et p2 sont équivalentes
# (toujours la même valeur)

p1 = input('Expression logique p1 (avec p et q) : ')
p2 = input('Expression logique p2 (avec p et q) : ')

equiv = True
print(f'  p\t  q\t  p1\t  p2')
for p in [True, False]:
    for q in [True, False]:
        v1 = eval(p1)
        v2 = eval(p2)
        print(f'{p}\t{q}\t {v1}\t {v2}')
        if v1 != v2:
            equiv = False

if equiv:
    print("Elles sont équivalentes !")
else:
    print("Elles NE sont PAS équivalentes !")
