#!/usr/bin/env python3

from random import randint

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
n = 0
tries = [ ]
win = False
while not win:
    ok = False 
    while not ok:
        guess = input('Votre idée: ')
        ok = guess.strip().isdecimal()
        #ok = guess.isdecimal()
        if not ok:
            print("Ce n'est pas un nombre... Réessayez.")
    guess = int(guess) 
    tries.append(guess)
    n += 1
    if guess == secret:
        print('Gagné ! En', n, 'coups !')
        win = True # donc not win sera False et on
                   # sortira de la boucle
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
print('Vos essais :', *tries)
