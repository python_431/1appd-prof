#!/usr/bin/env python3

from random import randint

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
n = 0
tries = [ ]
notfound = True 
while notfound:
    iswrong = True 
    while iswrong:
        guess = input('Votre idée: ')
        # càd: la chaîne n'est pas un nombre :
        iswrong = not guess.strip().isdecimal()
        #ok = guess.isdecimal()
        if iswrong:
            print("Ce n'est pas un nombre... Réessayez.")
    guess = int(guess) 
    tries.append(guess)
    n += 1
    if guess == secret:
        print('Gagné ! En', n, 'coups !')
        notfound = False # sortira de la boucle
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
print('Vos essais :', *tries)
