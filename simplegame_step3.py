
# Il est conseillé de mettre les import au début
# du script
from random import randint

name = input('Bonjour, quel est votre nom ? ')
print('Bonjour', name, '!')

# Étape 3
# on peut utiliser randint(1, 100)
# ou randrange(100) + 1

secret = randint(1, 100)

# code de test (on le vire ou le commente après)
print("J'ai tiré au sort :", secret)

# coder sur ce modèle votre step3.
# et testez qu'il marche en le lançant plusieurs
# fois.
