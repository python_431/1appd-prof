#!/usr/bin/env python3

from random import randint
from libgame import asknumber, save_game, read_high_scores, \
                    write_high_scores

# Ici on lit le fichier des high scores vraiment :
best_scores = read_high_scores('scores.txt')

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
score = 0
tries = [ ]
while True:
    guess = asknumber()  
    tries.append(guess)
    score += 1
    if guess == secret:
        print('Gagné ! En', score, 'coups !')
        break
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
print('Vos essais :', *tries)
save_game(name, score)

# partie C
if name in best_scores: # on est connu ?
    # alors, a-t-on fait-mieux que notre meilleur score
    # d'avant ?
    # le score actuel est dans n 
    # le score enregistré est dans best_score[player] 
    if score < best_scores[name]: # il a fait mieux, on met à jour scores
        best_scores[name] = score 
        print('Vous avez amélioré votre record !')
        write_high_scores('scores.txt', best_scores)
    else:
        print("Vous n'avez pas amélioré votre score, dommage...")
else: # on est pas connu : on entre dans le 'high scores'
    best_scores[name] = score 
    print('Vous entrez dans les high scores !')
    write_high_scores('scores.txt', best_scores)

