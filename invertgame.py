#!/usr/bin/env python

from random import randint

name = input('Quel est votre nom ? ')

print(name + ',', "merci de choisir un nombre entre 1 et 100.")
input("Appuyez sur entrée quand c'est fait...")

guess = randint(1,100)
#guess = 50 # mieux, mais moins "humain"
minimum = 1 
maximum = 100
n = 0

while True:
    print("Je propose guess :", guess)
    ok = False
    while not ok:
        answer = input("C'est + ou c'est - ou = ? ").strip()
        if answer == '+' or answer == '-' or answer == '=':
            ok = True
    # answer est nécessairement '+' ou '-' ou '=' ici...
    n += 1
    if answer == '=':
        print("J'ai gagné en", n, "coups !")
        break # on sort de la boucle while
    if answer == '+':
        if guess > minimum:
            minimum = guess
        guess = (guess + maximum)//2 
    else: # donc c'est -
        if guess < maximum:
            maximum = guess
        guess = (minimum + guess)//2
    if maximum <= minimum:
        print("Méchant humain ! Vous m'avez menti !")
        print("Je ne joue plus !")
        exit(42)

