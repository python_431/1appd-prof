#!/usr/bin/env python3

class City:
    def __init__(self, name, cp, pop):
        self.name = name
        self.cp   = cp
        self.pop  = pop
    def show(self):
        print(self.name, 'population', self.pop, 'code', self.cp)
    def __str__(self):
        return 'City ' + self.name 

v1 = City('Paris','75000', 2_000_000)
v2 = City('Versailles', '78000', 85_771)
v3 = City('Bordeaux', '33000', 249_712)

v2.show() # afficher Versaillle

l = [ v1, v2, v3 ]

for v in l:
    v.show() # devrait afficher Paris, Versaille, Bordeaux

print(v3)
