#!/usr/bin/env python3

# mystere1.py : que fait ce programme ?
# répondez en une phrase en français.
# Il demande un nombre et calcule la somme 
# des nombres de 1 au nombre saisi.

n = int(input('Nombre : '))

s = 0
for i in range(1, n+1): # va de 1 à n
    s += i              # ou s = s + i

# i va de 1 à n (un à la fois)
# on accumule les valeurs de i dans s
print(s)

# La formule que Gauss a trouvé
# pour 1 + 2 + 3 + ... + n
#print( int(n*(n+1)/2) )

