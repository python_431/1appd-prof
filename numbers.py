#!/usr/bin/env python

# on crée deux dictionnaires vides

# on crée les deux dictionnaires vides 
en_numbers = {}
fr_numbers = {}
bz_numbers = {} # j'ai le breton en plus
ru_numbers = {} # j'ai le russe en plus

with open('nombres.txt') as fd:
    # on lit chaque ligne
    for line in fd:
        # line contient une chaîne de la forme
        # 'sept seven семь 7'
        # on extrait dans french, english, russian, value
        french,english,breton,russian,value = line.strip().split() 
        value = int(value) # ça passera !
        # on insère l'entrée dans chaque dictionnaire
        fr_numbers[french]  = value
        en_numbers[english] = value
        bz_numbers[breton]  = value
        ru_numbers[russian] = value

#print(fr_numbers)
#print(en_numbers)
#print(bz_numbers)
#print(ru_numbers)

word = input('Tapez un nombre (1-10) en toute lettres : ')

if word in fr_numbers:
    print("C'est", fr_numbers[word], "en français.")
elif word in en_numbers:
    print("C'est", en_numbers[word], "en anglais.")
elif word in bz_numbers:
    print("C'est", bz_numbers[word], "en breton.")
elif word in ru_numbers:
    print("C'est", ru_numbers[word], "en russe.")
else:
    print("Je ne connais pas...")

