#!/usr/bin/env python3

from random import randint

# Demande une saisie et si elle représente
# un nombre entier, fait la conversion en entier
# et le renvoie, sinon redemande une saisie ...

def asknumber():
    ok = False
    while not ok:
        n  = input('Votre idée: ')
        ok = n.strip().isdecimal()
        if not ok:
            print("Ce n'est pas un nombre... Réessayez.")
    return int(n)

# Sauvegarde de la partie terminée à la fin
# du jeu : <nom du joueur>,<score> en l'ajoutant
# à la fin du fichier games.txt
def save_game(name, score):
    with open('games.txt', 'a') as out:
        out.write(name + ',' + str(score) + '\n')

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
n = 0
tries = [ ]
while True:
    guess = asknumber()  
    tries.append(guess)
    n += 1
    if guess == secret:
        print('Gagné ! En', n, 'coups !')
        break
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
print('Vos essais :', *tries)
save_game(name, n)
