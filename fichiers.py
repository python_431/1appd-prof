#!/usr/bin/env python




with open('data.txt') as finput:
    for name in finput:
        print(name.strip().upper())

# ceci fonctionne aussi :

finput = open('data.txt')
for name in finput:
    print(name.strip().upper())

# with est MIEUX, utilisez le.

# Pourquoi "with" ? 
# - with fait en sorte que le bloc qui suis (dans
# notre cas la boucle for qui lit le contenu du
# fichier) ne soit pas exécutée si le fichier ne
# peut pas être ouvert (il existe pas ou on n'a
# pas la permission ou il existe mais c'est un
# répertoire)
# - si le fichier a pu être ouvert, le bloc est
# exécuté et à la fin le fichier est fermé.

nom = 'John'
with open('output.txt', 'w') as out:
    out.write('Bonjour\n')
    out.write(nom + '\n')


