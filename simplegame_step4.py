
# Il est conseillé de mettre les import au début
# du script
from random import randint

name = input('Bonjour, quel est votre nom ? ')
print('Bonjour', name, '!')

# Étape 3
# on peut utiliser randint(1, 100)
# ou randrange(100) + 1

secret = randint(1, 100)

# code de test (on le vire ou le commente après)
##print("J'ai tiré au sort :", secret)

print('Essayez de deviner mon nombre entre 1 et 100 !')

# coder sur ce modèle votre step3.
# et testez qu'il marche en le lançant plusieurs
# fois.

#
n = 0

# étape 4

while True:
    # Demander un nombre au joueur
    # le mettre dans guess
    guess = int(input('Votre idée : '))
    n = n + 1
    # est-ce que c'est le bon ?
    if guess == secret:
        print('Gagné en', n, 'coups')
        # puisque c'est gagné, on sort de
        # la boucle :
        break
    # TODO: non, il est plus grand

    
    # TODO: non, il est plus petit
    
print('Bravo !')
