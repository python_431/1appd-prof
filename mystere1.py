#!/usr/bin/env python3

# mystere1.py : que fait ce programme ?
# répondez en une phrase en français.
# Il demande un nombre et calcule la somme 
# _ _ _ _ _ _.
# devinez puis vérifiez

n = int(input('Nombre : '))

s = 0
i = 1
while i <= n: # rappel : <= est "plus petit ou égal"
    s += i    # ou s = s + i
    i += 1    # ou i = i + 1

# i va de 1 à n (un à la fois)
# on accumule les valeurs de i dans s
print(s)

# La formule que Gauss a trouvé
# pour 1 + 2 + 3 + ... + n
print( int(n*(n+1)/2) )

