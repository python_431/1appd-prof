#!/usr/bin/env python3

# mystere2.py : que fait ce programme ?
# devinez puis vérifiez
# répète l'entrée en majuscule,
# indéfiniment sauf si je dis 
# un truc qui commence par END
# alors il se termine en me disant
# au revoir...
while True:
    txt = input('Texte : ')
    if txt.startswith('END'):
        break
    print(txt.upper())

print('Au revoir...')
