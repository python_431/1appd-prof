#!/usr/bin/env python3

from random import randint

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
n = 0

while True:
    guess = int(input('Votre idée: '))
    n += 1
    if guess == secret:
        print('Gagné ! En', n, 'coups !')
        break
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
