#!/usr/bin/env python3

from random import randint
from pathlib import Path


# Demande une saisie et si elle représente
# un nombre entier, fait la conversion en entier
# et le renvoie, sinon redemande une saisie ...

def asknumber():
    ok = False
    while not ok:
        n  = input('Votre idée: ')
        ok = n.strip().isdecimal()
        if not ok:
            print("Ce n'est pas un nombre... Réessayez.")
    return int(n)

# Sauvegarde de la partie terminée à la fin
# du jeu : <nom du joueur>,<score> en l'ajoutant
# à la fin du fichier games.txt
def save_game(name, score):
    with open('games.txt', 'a') as out: 
        out.write(name + ',' + str(score) + '\n')

def read_high_scores(filename):
    ## Crée le fichier de scores s'il n'existe pas
    if not Path('scores.txt').is_file(): # le fichier n'existe pas
        with open('scores.txt','w'): # crée le fichier
            pass # ne rien faire
    # Ici le fichier existe (si c'était pas le cas on vient
    # de le créer vide)
    best_scores = {} # cf. numbers.py 
    with open('scores.txt') as scorefile:
        for line in scorefile:
            player, best  = line.strip().split(',')
            best_scores[player] = int(best)
    return best_scores

def write_high_scores(filename, best_scores):
    with open('scores.txt','w') as scorefile:
        for player,best in best_scores.items():
            scorefile.write(player + ',' + str(best) + '\n')
            print(player, best)


# Ici on lit le fichier des high scores vraiment :
best_scores = read_high_scores('scores.txt')

name = input('Bonjour, quel est votre nom ? ')

print('Bonjour,', name, '!')
print('Essayez de deviner mon nombre entre 1 et 100 !')

secret = randint(1, 100)
score = 0
tries = [ ]
while True:
    guess = asknumber()  
    tries.append(guess)
    score += 1
    if guess == secret:
        print('Gagné ! En', score, 'coups !')
        break
    if guess > secret:
        print('Trop grand...')
    else:
        print('Trop petit...')

print('Bravo !')
print('Vos essais :', *tries)
save_game(name, score)

# partie C
if name in best_scores: # on est connu ?
    # alors, a-t-on fait-mieux que notre meilleur score
    # d'avant ?
    # le score actuel est dans n 
    # le score enregistré est dans best_score[player] 
    if score < best_scores[name]: # il a fait mieux, on met à jour scores
        best_scores[name] = score 
        print('Vous avez amélioré votre record !')
    else:
        print('Vous avez déjà fait mieux, dommage...')
else: # on est pas connu : on entre dans le 'high scores'
    best_scores[name] = score 
    print('Vous entrez dans les high scores !')

write_high_scores('scores.txt', best_scores)
