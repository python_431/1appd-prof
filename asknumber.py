#!/usr/bin/env python3
# fichier asknumber.py
# au départ, c'est pas ok...
ok = False
# tant que c'est pas ok
while not ok: # répète ce qui suit tant que not ok est
              # vraie c'est-à-dire que ok est faux
    # on demande du texte (str)
    guess = input('Je veux un nombre : ')
    # c'est ok (que des chiffres)
    ok = guess.isdecimal() # basculera ou pas la prochaine
                           # fois : sortir du while
    # sinon on alerte l'utilisateur
    #if not ok:
    #    print("Ce n'est pas un nombre... Réessayez.")

# on sortira ici si ok est vrai (not ok est faux)

print("Vous avez entré :", guess)

# lisez, reprenez ce code, exécutez 
# une fois que vous l'avez compris vous pouvez
# le reprendre dans simplegame2.py au bon endroit

while True: # boucle principale du jeu
    ok = False
    while not ok: 
        # entrée du bloc de la boucle
        guess = input(...) # pas de int, ça peut foirer
        ok = guess.isnumeric() # c'est des chiffres ? 
        if not ok:
            print('...') # on avertit le joueur si pb.
        # si ok est faux ça reboucle au début du bloc 

    # ici on est bon guess est numérique
    guess = int(guess) # maintenant on peut
    # égal (break)/plus grand/plus petit/ comme avant
    # reste de la logique du jeu est inchangé...

